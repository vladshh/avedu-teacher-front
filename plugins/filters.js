import Vue from "vue";

Vue.filter("number", (data, fix = 2) => {
  try {
    return data.toFixed(fix);
  } catch (error) {}

  return data;
});

Vue.filter("spaceBetweenNumber", (data) => {
  try {
    if (data) return data.toString().replace(/\B(?=(\d{3})+(?!\d))/g, " ");
    return "";
  } catch (error) {}

  return "";
});

Vue.filter("date", (val) => {
  try {
    if (val && typeof val == "string") {
      let date = new Date(val);
      return date.toLocaleDateString();
    }
  } catch (error) {}

  return val;
});

Vue.filter("datetime", (val) => {
  try {
    if (val && typeof val == "string") {
      let date = new Date(val);
      return date.toLocaleString();
    }
  } catch (error) {}
  return val;
});

Vue.filter("mmSecondToSecond", (val) => {
  try {
    if (!val) {
      return "00:00";
    }
    let arrTime = [];
    arrTime = val.split(":");
    return arrTime[0] + ":" + arrTime[1];
  } catch (error) {}
  return val;
});

Vue.filter("secondToMinute", (val) => {
  try {
    if (val <= 0) {
      return "00:00";
    }
    const format = (val) => `0${Math.floor(val)}`.slice(-2);
    const minutes = (val % 3600) / 60;
    return [minutes, val % 60].map(format).join(":");
  } catch (error) {}
  return val + " с.";
});
