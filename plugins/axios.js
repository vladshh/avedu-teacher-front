import { getUser } from "~/api/userApi";

export default ({ $axios, app }) => {
  $axios.setHeader("Accept", "application/json");

  if (app.i18n.locale) {
    $axios.setHeader("localization", app.i18n.locale);
  }

  if (app.$cookiz.get("token")) {
    $axios.setToken(app.$cookiz.get("token"), "Bearer");
  }
  if (app.$cookiz.get("auth.bid")) {
    $axios.setHeader("bid", app.$cookiz.get("auth.bid"));
  }
  // $axios.defaults.timeout = 90;
  $axios.interceptors.request.use(
    (config) => {
      return config;
    },
    (error) => {
      return Promise.reject(error);
    }
  );

  $axios.interceptors.response.use(
    (response) => {
      return response;
    },
    (error) => {
      if (app.store) {
        if (error.response.status == "401") {
        } else if (error.response.status == "403") {
          let user = JSON.parse(JSON.stringify(app.store.state.user));
          user["is_active"] = false;
          app.store.dispatch("setUser", user);
        } else if (error.response.status == "500") {
        }
      }
      if (process.server) {
        // $axios.interceptors.response.eject(interceptor);
      }

      return Promise.reject(error);
    }
  );
};
