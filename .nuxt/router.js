import Vue from 'vue'
import Router from 'vue-router'
import { normalizeURL, decode } from 'ufo'
import { interopDefault } from './utils'
import scrollBehavior from './router.scrollBehavior.js'

const _752d249a = () => interopDefault(import('../pages/customers/index.vue' /* webpackChunkName: "pages/customers/index" */))
const _14c01019 = () => interopDefault(import('../pages/dashboard/index.vue' /* webpackChunkName: "pages/dashboard/index" */))
const _68e1d8a4 = () => interopDefault(import('../pages/login/index.vue' /* webpackChunkName: "pages/login/index" */))
const _35595a76 = () => interopDefault(import('../pages/pupils/index.vue' /* webpackChunkName: "pages/pupils/index" */))
const _75f06954 = () => interopDefault(import('../pages/register/index.vue' /* webpackChunkName: "pages/register/index" */))
const _6ba89a13 = () => interopDefault(import('../pages/restore/index.vue' /* webpackChunkName: "pages/restore/index" */))
const _ac77bc78 = () => interopDefault(import('../pages/schools/index.vue' /* webpackChunkName: "pages/schools/index" */))
const _7437d258 = () => interopDefault(import('../pages/settings/index.vue' /* webpackChunkName: "pages/settings/index" */))
const _3caa3358 = () => interopDefault(import('../pages/index.vue' /* webpackChunkName: "pages/index" */))
const _52744a61 = () => interopDefault(import('../pages/pupils/_id/index.vue' /* webpackChunkName: "pages/pupils/_id/index" */))

const emptyFn = () => {}

Vue.use(Router)

export const routerOptions = {
  mode: 'history',
  base: '/',
  linkActiveClass: 'nuxt-link-active',
  linkExactActiveClass: 'nuxt-link-exact-active',
  scrollBehavior,

  routes: [{
    path: "/customers",
    component: _752d249a,
    name: "customers___ru"
  }, {
    path: "/dashboard",
    component: _14c01019,
    name: "dashboard___ru"
  }, {
    path: "/login",
    component: _68e1d8a4,
    name: "login___ru"
  }, {
    path: "/pupils",
    component: _35595a76,
    name: "pupils___ru"
  }, {
    path: "/register",
    component: _75f06954,
    name: "register___ru"
  }, {
    path: "/restore",
    component: _6ba89a13,
    name: "restore___ru"
  }, {
    path: "/schools",
    component: _ac77bc78,
    name: "schools___ru"
  }, {
    path: "/settings",
    component: _7437d258,
    name: "settings___ru"
  }, {
    path: "/uz",
    component: _3caa3358,
    name: "index___uz"
  }, {
    path: "/uz/customers",
    component: _752d249a,
    name: "customers___uz"
  }, {
    path: "/uz/dashboard",
    component: _14c01019,
    name: "dashboard___uz"
  }, {
    path: "/uz/login",
    component: _68e1d8a4,
    name: "login___uz"
  }, {
    path: "/uz/pupils",
    component: _35595a76,
    name: "pupils___uz"
  }, {
    path: "/uz/register",
    component: _75f06954,
    name: "register___uz"
  }, {
    path: "/uz/restore",
    component: _6ba89a13,
    name: "restore___uz"
  }, {
    path: "/uz/schools",
    component: _ac77bc78,
    name: "schools___uz"
  }, {
    path: "/uz/settings",
    component: _7437d258,
    name: "settings___uz"
  }, {
    path: "/uz/pupils/:id",
    component: _52744a61,
    name: "pupils-id___uz"
  }, {
    path: "/pupils/:id",
    component: _52744a61,
    name: "pupils-id___ru"
  }, {
    path: "/",
    component: _3caa3358,
    name: "index___ru"
  }],

  fallback: false
}

export function createRouter (ssrContext, config) {
  const base = (config._app && config._app.basePath) || routerOptions.base
  const router = new Router({ ...routerOptions, base  })

  // TODO: remove in Nuxt 3
  const originalPush = router.push
  router.push = function push (location, onComplete = emptyFn, onAbort) {
    return originalPush.call(this, location, onComplete, onAbort)
  }

  const resolve = router.resolve.bind(router)
  router.resolve = (to, current, append) => {
    if (typeof to === 'string') {
      to = normalizeURL(to)
    }
    return resolve(to, current, append)
  }

  return router
}
