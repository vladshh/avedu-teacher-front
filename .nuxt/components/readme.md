# Discovered Components

This is an auto-generated list of components discovered by [nuxt/components](https://github.com/nuxt/components).

You can directly use them in pages and other components without the need to import them.

**Tip:** If a component is conditionally rendered with `v-if` and is big, it is better to use `Lazy` or `lazy-` prefix to lazy load.

- `<IoSocketStatus>` | `<io-socket-status>` (node_modules/nuxt-socket-io/lib/components/SocketStatus.js)
- `<Calendar>` | `<calendar>` (components/Calendar.vue)
- `<Intro>` | `<intro>` (components/Intro.vue)
- `<Table>` | `<table>` (components/Table.vue)
- `<Timetable>` | `<timetable>` (components/Timetable.vue)
- `<Login>` | `<login>` (components/Auth/Login.vue)
- `<Register>` | `<register>` (components/Auth/Register.vue)
- `<Restore>` | `<restore>` (components/Auth/Restore.vue)
- `<SmsVerify>` | `<sms-verify>` (components/Auth/SmsVerify.vue)
- `<AddCustomer>` | `<add-customer>` (components/Customer/AddCustomer.vue)
- `<CustomerDialogue>` | `<customer-dialogue>` (components/Customer/CustomerDialogue.vue)
- `<CustomerTelVerification>` | `<customer-tel-verification>` (components/Customer/CustomerTelVerification.vue)
- `<Logo>` | `<logo>` (components/Header/Logo.vue)
- `<MenuList>` | `<menu-list>` (components/Header/MenuList.vue)
- `<Navigation>` | `<navigation>` (components/Header/Navigation.vue)
- `<Profile>` | `<profile>` (components/Header/Profile.vue)
- `<SetLanguage>` | `<set-language>` (components/Header/SetLanguage.vue)
- `<ChangePassword>` | `<change-password>` (components/Settings/ChangePassword.vue)
- `<ProfileInfo>` | `<profile-info>` (components/Settings/ProfileInfo.vue)
- `<SettingsCard>` | `<settings-card>` (components/Settings/SettingsCard.vue)
- `<MySelector>` | `<my-selector>` (components/UI/mySelector.vue)
