export { default as IoSocketStatus } from '../../node_modules/nuxt-socket-io/lib/components/SocketStatus.js'
export { default as Calendar } from '../../components/Calendar.vue'
export { default as Intro } from '../../components/Intro.vue'
export { default as Table } from '../../components/Table.vue'
export { default as Timetable } from '../../components/Timetable.vue'
export { default as Login } from '../../components/Auth/Login.vue'
export { default as Register } from '../../components/Auth/Register.vue'
export { default as Restore } from '../../components/Auth/Restore.vue'
export { default as SmsVerify } from '../../components/Auth/SmsVerify.vue'
export { default as AddCustomer } from '../../components/Customer/AddCustomer.vue'
export { default as CustomerDialogue } from '../../components/Customer/CustomerDialogue.vue'
export { default as CustomerTelVerification } from '../../components/Customer/CustomerTelVerification.vue'
export { default as Logo } from '../../components/Header/Logo.vue'
export { default as MenuList } from '../../components/Header/MenuList.vue'
export { default as Navigation } from '../../components/Header/Navigation.vue'
export { default as Profile } from '../../components/Header/Profile.vue'
export { default as SetLanguage } from '../../components/Header/SetLanguage.vue'
export { default as ChangePassword } from '../../components/Settings/ChangePassword.vue'
export { default as ProfileInfo } from '../../components/Settings/ProfileInfo.vue'
export { default as SettingsCard } from '../../components/Settings/SettingsCard.vue'
export { default as MySelector } from '../../components/UI/mySelector.vue'

// nuxt/nuxt.js#8607
function wrapFunctional(options) {
  if (!options || !options.functional) {
    return options
  }

  const propKeys = Array.isArray(options.props) ? options.props : Object.keys(options.props || {})

  return {
    render(h) {
      const attrs = {}
      const props = {}

      for (const key in this.$attrs) {
        if (propKeys.includes(key)) {
          props[key] = this.$attrs[key]
        } else {
          attrs[key] = this.$attrs[key]
        }
      }

      return h(options, {
        on: this.$listeners,
        attrs,
        props,
        scopedSlots: this.$scopedSlots,
      }, this.$slots.default)
    }
  }
}
