import Vuex from "vuex";
import Cookie from "js-cookie";
import { getUser, getProfile, getBusinessId, logIn } from "~/api/userApi";
import { getSchools, getSystem } from "~/api/systemApi";
import { getStats } from "~/api/statsApi";

import createPersistedState from "vuex-persistedstate";
const createStore = () => {
  return new Vuex.Store({
    state: {
      // user: null,
      // token: null,
      // bid: null,
      // routes: null,
      // root: null,
      // business: null,
      text: {},
      pageBreadcrumbs: [],
      selectedClass: null,

      // accounts: null,
      locations: null,
      categories: null,
      // schools: null,
    },
    mutations: {
      setPosts(state, posts) {
        state.loadedPosts = posts;
      },

      setToken(state, token) {
        state.token = token;
      },
      clearToken(state) {
        state.token = null;
      },
      clearUser(state) {
        state.user = null;
      },
      clearRoutes(state) {
        state.routes = null;
      },
      clearBusiness(state) {
        state.business = null;
      },
      clearAccounts(state) {
        state.accounts = null;
      },
      setBid(state, bid) {
        state.bid = bid;
      },
      clearBid(state) {
        state.bid = null;
      },
      SET_USER(state, payload) {
        state.user = payload;
      },
      SET_ACCOUNTS(state, payload) {
        state.accounts = payload.data;
      },
      SET_SELECTEDCLASS(state, payload) {
        state.selectedClass = payload;
      },
      SET_LANGUAGE(state, payload) {
        state.language = payload;
      },
      SET_BREADCRUMBS(state, payload) {
        state.pageBreadcrumbs = payload;
      },
      SET_MESSAGE(state, payload) {
        state.message = payload;
      },
      SET_CHAT_LIST(state, payload) {
        state.chat_list = payload;
      },
      SET_CATEGORIES(state, payload) {
        state.categories = payload;
      },
      SET_SCHOOLS(state, payload) {
        state.schools = payload;
      },
      SET_TEXT(state, payload) {
        state.text = payload;
      },
      SET_LOCATIONS(state, payload) {
        state.locations = payload;
      },
      SET_LOADING(state, payload) {
        state.globalLoading = payload;
      },
      setUnreadMsg(state, payload) {
        state.unreadMsg = payload;
      },
      SET_ROOT: (state, payload) => {
        state.root = payload;
      },
      SET_ROLES: (state, payload) => {
        state.roles = payload;
      },
      SET_ROLE: (state, payload) => {
        state.roles = payload;
      },
      SET_ROUTES: (state, payload) => {
        state.routes = payload;
      },
      SET_BUSINESS: (state, payload) => {
        state.business = payload;
      },
    },
    actions: {
      async nuxtServerInit(
        { dispatch, commit },
        { req, app, $axios, ...context }
      ) {
        let locale = "ru";

        const linkLang = await app.i18n.localeProperties.code;
        const cookieLang = await app.i18n.getLocaleCookie();

        if (cookieLang) {
          locale = cookieLang;
        } else if (linkLang) {
          locale = linkLang;
        }
        // let schools = await getSchools($axios);
        // if (schools && schools.data) {
        //   dispatch("setSchools", schools.data);
        // }
        app.i18n.setLocale(locale);
        app.i18n.setLocaleCookie(locale);
        dispatch("initLanguage", locale);

        let system = await getSystem($axios);
        if (system && system.text) {
          dispatch("setText", system.text);
        }
        if (system && system.locations) {
          dispatch("setLocations", system.locations);
        }
        if (system && system.categories) {
          dispatch("setCategories", system.categories);
        }

        if (app.$cookiz.get("auth.bid")) {
          console.log("REEEEEEEEQ ", app.$cookiz.get("auth.bid"));

          $axios.defaults.headers.common["bid"] = app.$cookiz.get("auth.bid");

          const user = await getUser($axios);
          this.$auth.$storage.setUniversal("user", user.data);
          let stats = await getStats($axios);
          this.$auth.$storage.setUniversal("stats", stats.data);
        }
      },
      setUser({ commit }, payload) {
        commit("SET_USER", payload);
      },
      setAccounts({ commit }, payload) {
        commit("SET_ACCOUNTS", payload);
      },
      SET_SELECTEDCLASS({ commit }, payload) {
        commit("SET_SELECTEDCLASS", payload);
      },
      setBreadcrumbs({ commit }, payload) {
        commit("SET_BREADCRUMBS", payload);
      },
      setMessage({ commit }, payload) {
        commit("SET_MESSAGE", payload);
      },
      setChatList({ commit }, payload) {
        commit("SET_CHAT_LIST", payload);
      },
      setCategories({ commit }, payload) {
        commit("SET_CATEGORIES", payload);
      },
      setSchools({ commit }, payload) {
        commit("SET_SCHOOLS", payload);
      },
      setText({ commit }, payload) {
        commit("SET_TEXT", payload);
      },
      setLocations({ commit }, payload) {
        commit("SET_LOCATIONS", payload);
      },

      initLanguage({ commit }, payload) {
        commit("SET_LANGUAGE", payload);
      },

      setLoading({ commit }, payload) {
        commit("SET_LOADING", payload);
      },
      setRoles({ commit }, payload) {
        commit("SET_ROLE", payload);
      },
      setRoutes({ commit }, payload) {
        commit("SET_ROUTES", payload);
      },
      setBusiness({ commit }, payload) {
        commit("SET_BUSINESS", payload);
      },
      setSchedule({ commit }, payload) {
        commit("SET_SCHEDULE", payload);
      },
      setRoot({ commit }, payload) {
        commit("SET_ROOT", payload);
      },
    },
    getters: {
      selectedClass(state) {
        return state.selectedClass;
      },
    },
  });
};

export default createStore;
