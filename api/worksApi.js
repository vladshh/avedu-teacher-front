import { catchNotify, toFormData } from "./_helper";

export function getWorks(axios, params = {}, id = null) {
  return axios({
    method: "get",
    url: `content/works${id ? `/${id}` : ""}`,
    params: params,
  })
    .then((response) => {
      return response.data;
    })
    .catch(() => {
      return null;
    });
}

export function setWorks(app, id, data) {

  return app
    .$axios({
      method: "post",
      data: toFormData(data),
      url: `content/works/${id}`,
      headers: {
        "Content-Type": "multipart/form-data",
      },
    })
    .then((response) => {
      return response.data;
    })
    .catch((error) => {
      return catchNotify(app.$vToastify, error);
    });
}

export function setAnswers(app, work_id, question_id, data) {
  return app
    .$axios({
      method: "post",
      data: toFormData(data),
      url: `content/works/${work_id}/questions/${question_id}/answers`,
      headers: {
        "Content-Type": "multipart/form-data",
      },
    })
    .then((response) => {
      return response.data;
    })
    .catch((error) => {
      return catchNotify(app.$vToastify, error);
    });
}
