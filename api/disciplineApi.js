import { catchNotify, toFormData } from "./_helper";

export function getDisciplines(axios, params = {}) {
  return axios({
    method: "get",
    url: `content/disciplines`,
    params: params,
  })
    .then((response) => {
      return response.data;
    })
    .catch(() => {
      return null;
    });
}

export function getLessonplans(axios, discipline_id, params = {}, id) {
  return axios({
    method: "get",
    url: `content/disciplines/${discipline_id}/lessonplans${
      id ? `/${id}` : ""
    }`,
    params: params,
  })
    .then((response) => {
      return response.data;
    })
    .catch(() => {
      return null;
    });
}
