import { catchNotify, toFormData } from "./_helper";

export function logIn(app, data) {
  return app
    .$axios({
      method: "post",
      data: toFormData(data),
      url: `auth/login`,
      headers: {
        "Content-Type": "multipart/form-data",
      },
    })
    .then((response) => {
      return response.data;
    })
    .catch((error) => {
      return catchNotify(app.$vToastify, error);
    });
}

export function register(app, data) {
  return app
    .$axios({
      method: "post",
      data: toFormData(data),
      url: `auth/register`,
      headers: {
        "Content-Type": "multipart/form-data",
      },
    })
    .then((response) => {
      return response.data;
    })
    .catch((error) => {
      return catchNotify(app.$vToastify, error);
    });
}
export function addTeacher(app, data) {
  return app
    .$axios({
      method: "post",
      data: toFormData(data),
      url: `business/customers`,
      headers: {
        "Content-Type": "multipart/form-data",
      },
    })
    .then((response) => {
      return response.data;
    })
    .catch((error) => {
      return catchNotify(app.$vToastify, error);
    });
}

export function editTeacher(app, data) {
  data["_method"] = "PUT";
  return app
    .$axios({
      method: "POST",
      data: toFormData(data),

      url: `business/customers/${data.id}`,
      headers: {
        "Content-Type": "multipart/form-data",
      },
    })
    .then((response) => {
      return response.data;
    })
    .catch((error) => {
      return catchNotify(app.$vToastify, error);
    });
}
export function restore(app, data) {
  return app
    .$axios({
      method: "post",
      data: toFormData(data),
      url: `auth/restore`,
      headers: {
        "Content-Type": "multipart/form-data",
      },
    })
    .then((response) => {
      return response.data;
    })
    .catch((error) => {
      return catchNotify(app.$vToastify, error);
    });
}
export function sendSms(app, data) {
  return app
    .$axios({
      method: "post",
      data: toFormData(data),
      url: `auth/code/`,
      headers: {
        "Content-Type": "multipart/form-data",
      },
    })
    .then((response) => {
      return response.data;
    })
    .catch((error) => {
      return catchNotify(app.$vToastify, error);
    });
}
export function verifySms(app, data) {
  return app
    .$axios({
      method: "post",
      data: toFormData(data),
      url: `auth/code/verify`,
      headers: {
        "Content-Type": "multipart/form-data",
      },
    })
    .then((response) => {
      return response.data;
    })
    .catch((error) => {
      return catchNotify(app.$vToastify, error);
    });
}
export function getStats(axios) {
  return axios({
    method: "get",
    url: `business/stats`,
  })
    .then((response) => {
      return response.data;
    })
    .catch(() => {
      return null;
    });
}
export function getTeacherGroups(axios) {
  return axios({
    method: "get",
    url: `business/teacher_groups`,
  })
    .then((response) => {
      return response.data;
    })
    .catch(() => {
      return null;
    });
}
export function getTeacherGroup(axios, id) {
  return axios({
    method: "get",
    url: `business/teacher_groups/${id}`,
  })
    .then((response) => {
      return response.data;
    })
    .catch(() => {
      return null;
    });
}
// export function getClass(axios, discipline_id, params = {}, id) {
//   return axios({
//     method: "get",
//     url: `content/disciplines/${discipline_id}/lessonplans${
//       id ? `/${id}` : ""
//     }`,
//     params: params,
//   })
//     .then((response) => {
//       return response.data;
//     })
//     .catch(() => {
//       return null;
//     });
// }
export function getClass(axios, discipline_id, params = {}, id) {
  return axios({
    method: "get",
    url: `business/teacher_groups/${discipline_id}/journals`,
    params: params,
  })
    .then((response) => {
      return response.data;
    })
    .catch(() => {
      return null;
    });
}
export function getCustomers(axios) {
  return axios({
    method: "get",
    url: `business/customers`,

    params: {
      per_page: 100,
    },
  })
    .then((response) => {
      return response.data;
    })
    .catch(() => {
      return null;
    });
}
export function getUser(axios) {
  return axios({
    method: "get",
    url: `profile`,
  })
    .then((response) => {
      return response.data;
    })
    .catch((error) => {
      return null;
    });
}
export function getUserWithBid(axios, bid = null) {
  return axios({
    method: "get",
    url: `profile${bid ? `/${bid}` : ""}`,
  })
    .then((response) => {
      return response.data;
    })
    .catch((error) => {
      return null;
    });
}

// export function getConference(axios, id = null) {
//   return axios({
//     method: "get",
//     url: `content/conferences${id ? `/${id}` : ""}`,
//   })
//     .then((response) => {
//       return response.data;
//     })
//     .catch(() => {
//       return null;
//     });
// }
export function getBusinessId(app, data) {
  return app
    .$axios({
      method: "get",
      url: `accounts`,
    })
    .then((response) => {
      return response.data;
    })
    .catch((error) => {
      return null;
    });
}
export function getAccs(axios, params = {}) {
  return axios({
    method: "get",
    url: `accounts`,
    params: params,
  })
    .then((response) => {
      return response.data;
    })
    .catch(() => {
      return null;
    });
}

export function updateUser(app, data) {
  return app
    .$axios({
      method: "post",
      data: toFormData(data),
      url: `profile`,
      headers: {
        "Content-Type": "multipart/form-data",
      },
    })
    .then((response) => {
      return response.data;
    })
    .catch((error) => {
      return catchNotify(app.$vToastify, error);
    });
}

// export function restore(app, data) {
//   return app
//     .$axios({
//       method: "post",
//       data: toFormData(data),
//       url: `auth/restore`,
//       headers: {
//         "Content-Type": "multipart/form-data",
//       },
//     })
//     .then((response) => {
//       return response.data;
//     })
//     .catch((error) => {
//       return catchNotify(app.$vToastify, error);
//     });
// }

// export function sendSms(app, data) {
//   return app
//     .$axios({
//       method: "post",
//       data: toFormData(data),
//       url: `auth/code/get`,
//       headers: {
//         "Content-Type": "multipart/form-data",
//       },
//     })
//     .then((response) => {
//       return response.data;
//     })
//     .catch((error) => {
//       return catchNotify(app.$vToastify, error);
//     });
// }

// export function verifySms(app, data) {
//   return app
//     .$axios({
//       method: "post",
//       data: toFormData(data),
//       url: `auth/code/verify`,
//       headers: {
//         "Content-Type": "multipart/form-data",
//       },
//     })
//     .then((response) => {
//       return response.data;
//     })
//     .catch((error) => {
//       return catchNotify(app.$vToastify, error);
//     });
// }
