import { catchNotify, toFormData } from "./_helper";

export function getReminders(axios, params) {
  return axios({
    method: "get",
    url: `content/reminders`,
    params: params,
  })
    .then((response) => {
      return response.data;
    })
    .catch(() => {
      return null;
    });
}

export function setReminders(app, data) {
  return app
    .$axios({
      method: "post",
      data: toFormData(data),
      url: `content/reminders`,
      headers: {
        "Content-Type": "multipart/form-data",
      },
    })
    .then((response) => {
      return response.data;
    })
    .catch((error) => {
      return catchNotify(app.$vToastify, error);
    });
}
