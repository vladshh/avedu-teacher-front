export function getSystem(axios) {
  return axios({
    method: "get",
    url: `system`,
  })
    .then((response) => {
      return response.data;
    })
    .catch(() => {
      return null;
    });
}

export function getSchools(axios) {
  return axios({
    method: "get",
    url: `businesses`,
  })
    .then((response) => {
      return response.data;
    })
    .catch(() => {
      return null;
    });
}
