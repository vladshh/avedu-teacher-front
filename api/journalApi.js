export function getJournal(axios, params = {}) {
  return axios({
    method: "get",
    url: `content/journal`,
    params: params,
  })
    .then((response) => {
      return response.data;
    })
    .catch(() => {
      return null;
    });
}
