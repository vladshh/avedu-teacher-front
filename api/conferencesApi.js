export function getConference(axios, id = null) {
  return axios({
    method: "get",
    url: `content/conferences${id ? `/${id}` : ""}`,
  })
    .then((response) => {
      return response.data;
    })
    .catch(() => {
      return null;
    });
}
