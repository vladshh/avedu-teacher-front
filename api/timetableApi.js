export function getTimetable(axios, params = {}) {
  return axios({
    method: "get",
    url: `content/timetable`,
    params: params,
  })
    .then((response) => {
      return response.data;
    })
    .catch(() => {
      return null;
    });
}
