export function getRating(axios, params = {}) {
  return axios({
    method: "get",
    url: `content/rating`,
    params: params,
  })
    .then((response) => {
      return response.data;
    })
    .catch(() => {
      return null;
    });
}
