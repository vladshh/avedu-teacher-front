export function getStats(axios, params = {}) {
  return axios({
    method: "get",
    url: `business/stats`,
    params: params,
  })
    .then((response) => {
      return response.data;
    })
    .catch(() => {
      return null;
    });
}
