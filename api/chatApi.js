import { catchNotify, toFormData } from "./_helper";

export function getChat(axios, params, id = null) {
  // console.log(params);
  return axios({
    method: "get",
    url: `content/chat${id ? `/${id}` : ""}`,
    params: params,
  })
    .then((response) => {
      return response.data;
    })
    .catch(() => {
      return null;
    });
}

export function sendMessage(app, data) {
  return app
    .$axios({
      method: "post",
      data: toFormData(data),
      url: `content/chat`,
      headers: {
        "Content-Type": "multipart/form-data",
      },
    })
    .then((response) => {
      return response.data;
    })
    .catch((error) => {
      return catchNotify(app.$vToastify, error);
    });
}
