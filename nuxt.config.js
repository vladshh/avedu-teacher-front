import colors from "vuetify/es5/util/colors";

export default {
  server: {
    port: 3005,
    // host: "localhost",
    host: process.env.HOST,

    // default: 3000,
    // host: "localhost", // default: localhost
  },
  auth: {
    strategies: {
      local: {
        token: {
          property: "token",
          global: true,
          type: "Bearer",
        },
        user: {
          property: false,
          autoFetch: false,
        },
        // redirect: {
        //   home: "/",
        //   login: "/login",
        //   logout: "/login",
        // },
        redirect: false,
        endpoints: {
          login: {
            url: "/auth/login",
            method: "post",
            // propertyName: "data.access.accessToken",
          },
          logout: { url: "/sessions", method: "delete" },
          // user: {
          //   url: "/profile",
          //   method: "get",
          //   // propertyName: "data.attributes",
          // },
          user: false,
        },
        cookie: {
          options: {
            expires: 1,
          },
        },
        // tokenRequired: true,
        // tokenType: "",
      },
    },
  },
  // Global page headers: https://go.nuxtjs.dev/config-head
  head: {
    titleTemplate: "%s - AVEDU",
    title: "AVEDU",
    meta: [
      { charset: "utf-8" },
      { name: "viewport", content: "width=device-width, initial-scale=1" },
      { hid: "description", name: "description", content: "" },
    ],
    link: [
      { rel: "icon", type: "image/x-icon", href: "/favicon.ico" },
      {
        rel: "stylesheet",
        href: "https://fonts.googleapis.com/css2?family=Inter:wght@100;200;300;400;500;600;700;800;900&display=swap",
      },
    ],
  },

  // Global CSS: https://go.nuxtjs.dev/config-css
  css: [
    {
      src: "~assets/scss/main.scss",
      lang: "scss",
    },
  ],

  loading: {
    color: "green",
    height: "5px",
    duration: 9000,
    failedColor: "red",
  },
  loadingIndicator: {
    name: "circle",
    color: "#3B8070",
    background: "white",
  },
  router: {
    middleware: ["auth"],
  },

  // Plugins to run before rendering page: https://go.nuxtjs.dev/config-plugins
  plugins: [
    { src: "~/plugins/ob.js", mode: "client" },
    { src: "~/plugins/filters.js", ssr: true },
    { src: "~/plugins/axios.js", ssr: true },
    { src: "~/plugins/toastr.js", mode: "client" },
    { src: "~/plugins/leader-line-vue.js", mode: "client" },
    { src: "~/plugins/scroll-chat.js", mode: "client" },
    { src: "~/plugins/datetime-picker.js", mode: "client" },
    { src: "~plugins/vue-the-mask.js", mode: "client" },
  ],

  // Auto import components: https://go.nuxtjs.dev/config-components
  components: [{ path: "~/components", pathPrefix: false }],

  // Modules for dev and build (recommended): https://go.nuxtjs.dev/config-modules
  buildModules: [
    // https://go.nuxtjs.dev/eslint
    "@nuxtjs/eslint-module",
    // https://go.nuxtjs.dev/vuetify
    "@nuxtjs/vuetify",
  ],

  // Modules: https://go.nuxtjs.dev/config-modules
  modules: [
    "@nuxtjs/axios",
    // https://go.nuxtjs.dev/pwa
    "@nuxtjs/pwa",
    "nuxt-socket-io",
    "nuxt-i18n",
    "@nuxtjs/auth-next",
    ["cookie-universal-nuxt", { alias: "cookiz", parseJSON: true }],
    [
      "@nuxtjs/yandex-metrika",
      {
        id: "90202109",
        clickmap: true,
        trackLinks: true,
        accurateTrackBounce: true,
        webvisor: true,
      },
    ],
  ],
  io: {
    // module options
    sockets: [
      {
        name: "main",
        url: process.env.SOCKET_URL,
      },
    ],
  },
  // Axios module configuration: https://go.nuxtjs.dev/config-axios
  axios: {
    baseURL: process.env.BASE_URL,
    withCredentials: true,
    // headers: {
    //   common: {
    //     bid: $auth.$storage.getUniversal("bid")
    //       ? $auth.$storage.getUniversal("bid")
    //       : null,
    //   },
    // },
  },
  // PWA module configuration: https://go.nuxtjs.dev/pwa
  pwa: {
    manifest: {
      lang: "ru",
    },
  },
  i18n: {
    strategy: "prefix_except_default",
    defaultLocale: "ru",
    locales: [
      {
        code: "uz",
        iso: "uz-UZ",
      },
      {
        code: "ru",
        iso: "ru-Ru",
      },
    ],
    seo: true,
    encodePaths: false,
  },

  // Vuetify module configuration: https://go.nuxtjs.dev/config-vuetify
  vuetify: {
    customVariables: ["~/assets/variables.scss"],
    treeShake: true,
    theme: {
      options: {
        customProperties: true,
      },
      themes: {
        light: {
          background: "#f5f5fb",
        },
      },
      dark: false,
    },
  },
  transition: {
    name: "fade",
    mode: "out-in",
  },
  // Build Configuration: https://go.nuxtjs.dev/config-build
  build: {
    filenames: {
      chunk: ({ isDev }) => (isDev ? "[name].js" : "[id].[contenthash].js"),
    },
    extractCSS: true,
    // transpile:['/^vue-pdf($|\/)/'],
    extend(config, ctx) {
      config.module.rules.push({
        enforce: "pre",
        test: /\.(js|vue)$/,
        loader: "eslint-loader",
        exclude: /(node_modules)/,
        options: {
          fix: true,
        },
      });
    },
  },
};
